# Dayaṁ Gallery

https://gallery.dayam.org/

## Usage

1. Install [nvm](https://github.com/nvm-sh/nvm). Then:

   ```sh
   git clone https://gitlab.com/dayam.org/gallery.git
   cd gallery
   nvm use
   npm install
   npm run setup
   ```

2. Configure `s3cmd` to access the `dayam-gallery` bucket.

   ```sh
   cp s3cmd-config.example s3cmd-config
   chmod 600 s3cmd-config
   vim s3cmd-config
   ```

3. Copy photos and videos into folders in `media`.

4. Build

   ```sh
   npm run build
   ```

5. Deploy

   ```sh
   npm run deploy
   ```

## References

- [thumbsup](https://thumbsup.github.io/)
- [S3cmd](https://s3tools.org/s3cmd)
- [Guide for using S3cmd with
  Linode Object Storage](https://www.linode.com/docs/products/storage/object-storage/guides/s3cmd/)
